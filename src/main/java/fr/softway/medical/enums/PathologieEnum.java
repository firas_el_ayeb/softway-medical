package fr.softway.medical.enums;

public enum PathologieEnum {
    CARDIAQUE("Cardiologie"),
    TRAUMATOLOGIE("Traumatologie"),
    CARDIAQUE_TRAUMATOLOGIE("Cardiologie, Traumatologie");

    private final String message;

    PathologieEnum(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
