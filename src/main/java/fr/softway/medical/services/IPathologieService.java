package fr.softway.medical.services;

public interface IPathologieService {
    public String getUniteMedicale(int index);
}
