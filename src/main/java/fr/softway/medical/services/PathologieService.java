package fr.softway.medical.services;

import fr.softway.medical.enums.PathologieEnum;
import org.springframework.stereotype.Service;

@Service
public class PathologieService implements IPathologieService {
    @Override
    public String getUniteMedicale(int index) {
        if (index % 15 == 0) {
            return PathologieEnum.CARDIAQUE_TRAUMATOLOGIE.getMessage();
        } else if (index % 3 == 0) {
            return PathologieEnum.CARDIAQUE.getMessage();
        } else if (index % 5 == 0) {
            return PathologieEnum.TRAUMATOLOGIE.getMessage();
        }
        return null;
    }
}
