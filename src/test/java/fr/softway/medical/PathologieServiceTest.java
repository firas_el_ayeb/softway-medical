package fr.softway.medical;

import fr.softway.medical.enums.PathologieEnum;
import fr.softway.medical.services.IPathologieService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class PathologieServiceTest {
    @Autowired
    private IPathologieService iPathologieService;

    @Test
    @DisplayName("Le système doit renvoyer Cardiologie.")
    void testCasCardiologie() {
        String uniteMedicale = iPathologieService.getUniteMedicale(33);
        assertThat(uniteMedicale).isEqualTo(PathologieEnum.CARDIAQUE.getMessage());
    }

    @Test
    @DisplayName("Le système doit renvoyer Traumatologie.")
    void testCasTraumatologie() {
        String uniteMedicale = iPathologieService.getUniteMedicale(55);
        assertThat(uniteMedicale).isEqualTo(PathologieEnum.TRAUMATOLOGIE.getMessage());
    }

    @Test
    @DisplayName("Le système doit renvoyer Cardiologie, Traumatologie.")
    void testCasCardiologieAndTraumatologie() {
        String uniteMedicale = iPathologieService.getUniteMedicale(15);
        assertThat(uniteMedicale).isEqualTo(PathologieEnum.CARDIAQUE_TRAUMATOLOGIE.getMessage());
    }
}
